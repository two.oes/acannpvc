package main

import(
	"net/http"
	"io/ioutil"
	"encoding/json"
	"log"
	corev1 "k8s.io/api/core/v1"
	admissionv1 "k8s.io/api/admission/v1"
//	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
      )

func (gs *myValidServerHandler) valserve(w http.ResponseWriter, r *http.Request) {
	
	var Body []byte
	if r.Body != nil {
		if data, err := ioutil.ReadAll(r.Body); err != nil {
			Body = data
		}
	}

	if len(Body) == 0 {
		log.Println("Unable to read Body, empty string")
		http.Error(w, "Unable to read Body, empty string", http.StatusBadRequest)
		return
	}

	if gs.debug == "yes" {
		log.Println("Data received")
	}

	if r.URL.Path != "/validate" {
		log.Println("Unable to process request , invalide Path provided")
		http.Error(w,"Unable to process request, invalide Path provided", http.StatusBadRequest)
		return
	}

	arRequest := admissionv1.AdmissionReview{}
	if err := json.Unmarshal(Body,arRequest); err != nil {
		log.Println("Unable to Unmarshal the Body request")
		http.Error(w,"Unable to Unmarshal the Body request", http.StatusBadRequest)
		return
	}

	raw := arRequest.Request.Object.Raw
	obj := corev1.PersistentVolumeClaim{}

	if err := json.Unmarshal(raw, &obj); err != nil {
		log.Println("Error , unable to Unmarshal the PVC object")
		http.Error(w, "Error, unable to Unmarshal the PVC object", http.StatusBadRequest)
		return
	}

//	arResponse := admissionv1.AdmissionReview {
//		Response: admissionv1.AdmissionResponse{
//			Result: &metav1.Status{Status: "Failure", Message: "The PVC does not have the selected-node annotation", Code: 401},
//			UID: arRequest.Request.UID,
//			Allowed: false,
//		},
//	}

	log.Printf("%v",obj)
}
