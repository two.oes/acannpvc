package main

import (
     "fmt"
     "log"
     "os"
     "flag"
     "crypto/tls"
     "net/http"
     "os/signal"
     "syscall"
     "context"
)

type myValidServerHandler struct {
	annoValue string
	debug string
}

var (
	tlscert, tlskey string
)

func getEnv(key, fallback string) string {
	value , exists := os.LookupEnv(key)
        if !exists {
	   value = fallback
        }
	return value
}

func main() {
	certfile := getEnv("CERT_FILE","/etc/certs/cert.pem")
	keyfile := getEnv("KEY_FILE","/etc/certs/key.pem")

	flag.StringVar(&tlscert, "tlsCertFile", certfile , "The File contains the X509 Certificate for TLS")
        flag.StringVar(&tlskey, "tlsKeyFile", keyfile , "The File contains the X509 Key for TLS")

	flag.Parse()

	certs, err := tls.LoadX509KeyPair(tlscert, tlskey)

	if err != nil {
		log.Println("Failed to Load Cert/Key Pair Certificate")
		os.Exit(3)
	}

	port := getEnv("PORT","8080")
	pvc_anno := getEnv("ANNOTATION_VALUE","none")
	debug_bol := getEnv("DEBUG","off")

	gs := myValidServerHandler{}
     mr := myValidServerHandler{}
	gs.annoValue = pvc_anno
	mr.annoValue = pvc_anno
	gs.debug = debug_bol
	mr.debug = debug_bol

	server := &http.Server {
	  Addr: fmt.Sprintf(":%v", port),
          TLSConfig: &tls.Config{Certificates: []tls.Certificate{certs}},
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/validate", gs.valserve)
     mux.HandleFunc("/mutate", mr.mutserve)
     server.Handler = mux

	go func() {
		if err = server.ListenAndServeTLS("",""); err != nil {
			log.Println("Failed to Listen And Serve With TLS")
		}
	}()

	fmt.Fprintf(os.Stdout, "The Server is running and Listening on Port : %s \n", port)

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan,syscall.SIGINT, syscall.SIGTERM)
	<-signalChan

	fmt.Fprintf(os.Stdout, "Get Shutdown Signal , Sutting down the webhook Server Gracefully\n")
	server.Shutdown(context.Background())
}
