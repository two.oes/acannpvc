package main

import(
	"io/ioutil"
	"net/http"
	"encoding/json"
	"log"
	corev1 "k8s.io/api/core/v1"
	admissionv1 "k8s.io/api/admission/v1"
      )

type patchOperation struct {
	Op string 	`json:"op"`
	Path string `json:"path"`
	Value interface{} `json:"value,omitempty"`
}

func (mr *myValidServerHandler) mutserve(w http.ResponseWriter, r *http.Request) {
	
	var Body []byte
	if r.Body != nil {
		if data , err := ioutil.ReadAll(r.Body); err == nil {
			Body = data
		}
	}

	if len(Body) == 0 {
		log.Println("Unable to manage Body , empty string")
		http.Error(w,"Unable to manage Body , empty string",http.StatusBadRequest)
		return
	}

	if mr.debug == "yes" {
		log.Println("Body Received")
	} 

	if r.URL.Path != "/mutate" {
		log.Println("Not a valid URL provided")
		http.Error(w,"Not a valid URL provided", http.StatusBadRequest)
		return
	}

	arRequest := admissionv1.AdmissionReview{}
	if err := json.Unmarshal(Body,arRequest); err != nil {
		log.Println("Unable to Unmarshal the Body request")
		http.Error(w,"Unable to Unmarshal the Body request", http.StatusBadRequest)
		return
	}

	raw := arRequest.Request.Object.Raw
	obj := corev1.PersistentVolumeClaim{}

	if err := json.Unmarshal(raw, &obj); err != nil {
		log.Println("Error , unable to Unmarshal the PVC object")
		http.Error(w, "Error, unable to Unmarshal the PVC object", http.StatusBadRequest)
		return
	}

	log.Printf("The PVC Name is %v", obj.Spec.VolumeName)
	
	var patches []patchOperation

	patches = append(patches, patchOperation{
				Op: "Add",
				Path:  "/metadata/annotations/volume.kubernetes.io/selected-node",
				Value: "bilbow.oichman.net",
	})

	arResponse := admissionv1.AdmissionReview{
		Response: &admissionv1.AdmissionResponse {
			UID: arRequest.Request.UID,
		},
	}

	patchBytes, err := json.Marshal(patches)

	if err != nil {
		log.Println("Unable to Marshal the Patch")
		http.Error(w,"Unable to Marshal the Patch", http.StatusBadRequest)
		return
	}

	v1JSONPatch := admissionv1.PatchTypeJSONPatch
	arResponse.APIVersion = "admission.k8s.io/v1"
	arResponse.Kind = arRequest.Kind
	arResponse.Response.Allowed = true
	arResponse.Response.Patch = patchBytes
	arResponse.Response.PatchType = &v1JSONPatch

	resp , rErr := json.Marshal(arResponse)

	if rErr != nil {
		log.Println("Can not Marshal the Response")
		http.Error(w,"Can not Marshal the Response", http.StatusBadRequest)
		return
	}

	if _ , wErr := w.Write(resp); wErr != nil {
		log.Println("Can not write the Response")
		http.Error(w,"Can not write the response", http.StatusBadRequest)
	}
}
